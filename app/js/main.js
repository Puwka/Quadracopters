var local = new Vue({
	el:"#my-header",
	data: {
		show: false,
		chose: 0,
		menus: [
			{
				info: "О нас",
				active: false
			},
			{
				info: "О вас",
				active: false
			},
			{
				info: "Галерея",
				active: false
			},
			{
				info: "Соц. Сети",
				active: false
			},
			{
				info: "Обратная связь",
				active: false
			}
		]
	},
		methods: {
			change: function(menu) {
				for (i = 0; i < this.menus.length; i++) {
					this.menus[i].active = false
				}
				menu.active = true
			}
		}
})
